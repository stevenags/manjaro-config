
"      :+#%@@@@%+.             
"    +@@%*=--:-*@@-             * My personal .vimrc config file 
"  .%@#:        %@*  .:-.       * Steven Agustinus  
"  %@%        -@@%. *@@@@*     
"  @@%        *#-  %@%.:@@=    
"  -@@=           =@@.  #@#    
"   -%@@+:        @@+   =@@.   
"     :*%@@%#+-. #@@    .@@+   
"         :=*%@@@@@-     %@%   
"              .%@@@.   .*@@++-
"             .-@@@@.-#@@@@@%#=
"  +####%%%@@@@@@@= :@@*. %@*  
"  -++++===--:=@@-   %@%. *@#  
"             *@%     %@%:*@%  
"             @@-      *@@@@- 

syntax on
filetype plugin on
set nocompatible
set clipboard=unnamedplus
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set termguicolors
set encoding=UTF-8
set laststatus=2
set guifont=FiraCode\ Nerd\ Font\ Mono\ 10

call plug#begin()
Plug 'nathanaelkane/vim-indent-guides'
Plug 'morhetz/gruvbox'
Plug 'joshdick/onedark.vim'
Plug 'Valloric/YouCompleteMe' "auto complete
Plug 'jiangmiao/auto-pairs'
Plug 'Vimjas/vim-python-pep8-indent'
Plug 'dense-analysis/ale'
Plug 'vimwiki/vimwiki'
Plug 'preservim/nerdtree'
Plug 'ryanoasis/vim-devicons'
Plug 'itchyny/lightline.vim' 
Plug 'vifm/vifm.vim'  
Plug 'francoiscabrol/ranger.vim'
Plug 'preservim/nerdcommenter' "to give a comment
Plug 'junegunn/fzf.vim' "fuzzy finder
Plug 'adelarsq/vim-matchit'
Plug 'vim-python/python-syntax'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'ap/vim-css-color'
Plug 'chrisbra/colorizer'
call plug#end()

"colorizer :
 
"let g:colorizer_auto_color = 1
let g:colorizer_auto_filetype='css,html,haskell'
"let g:colorizer_x11_names = 1
let g:colorizer_syntax = 1
"let g:colorizer_hex_pattern = ['#', '\%(\x\{3}\|\x\{6}\)', '']


"let g:gruvbox_italic=1
"let s:bg1  = s:gb.dark1
"colorscheme onedark
set ts=4 sw=4 et 
let g:indent_guides_start_level=2
let g:indent_guides_size=1
let g:AutoPairsFlyMode=1

"Re-binding esc key to kj 
inoremap kj <Esc>

"set leader key to <space>
let mapleader = "\<space>"

"set leader key for vertical split
nnoremap <Leader>vs :vs<CR>
nnoremap <Leader>hs :sp<CR>
nnoremap <tab> <c-w>
nnoremap <tab><tab> <c-w><c-w>

"keybinding for NerdTree
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

"disabled arrow key in keyboard
nnoremap <Up> <Nop>
nnoremap <Down> <Nop>
nnoremap <Left> <Nop>
nnoremap <Right> <Nop>

"==== Fzf config ===
"let g:fzf_command_prefix = 'Fzf'
"Preview window
"let g:fzf_preview_window = ['right:50%', 'ctrl-/']

